package me.libraryaddict.Hungergames.Configs;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class LilypadConfig extends BaseConfig
{
    /**
     * The lobbies that players should be teleported to when the get kicked or the server shuts down.
     */
    private String[] lobbies = new String[]{"Lobby1", "Lobby2", "Lobby3"};

    public LilypadConfig()
    {
        super("lilypad");
    }
}
