package me.libraryaddict.Hungergames.Managers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.entity.Player;

import com.sendev.lilyserverhook.api.ServerHookAPI;
import me.libraryaddict.Hungergames.Configs.LilypadConfig;
import me.libraryaddict.Hungergames.Hungergames;
import me.libraryaddict.Hungergames.Types.HungergamesApi;
import me.libraryaddict.Hungergames.Utilities.MapLoader;

public class LilypadManager
{
    private static final List<String> lobbies = new ArrayList<>();
    private static final Random random = new Random();

    static {
        showServer();
        setupLobbies();
    }

    private static void setupLobbies()
    {
        LilypadConfig config = HungergamesApi.getConfigManager().getLilypadConfig();

        for (String lobby : config.getLobbies()) {
            if (lobbies.contains(lobby)) {
                continue;
            }

            lobbies.add(lobby);
        }
    }

    public static void setupPlaceholders()
    {
        Hungergames plugin = HungergamesApi.getHungergames();

        // Sets up {gameTime} to display how much time is left
        ServerHookAPI.registerDataPlaceholder(plugin, "gameTime", () -> {
            return plugin.returnTime(plugin.currentTime);
        });

        // Sets up {gameMap} to display the map name
        ServerHookAPI.registerDataPlaceholder(plugin, "gameMap", () -> {
            if (MapLoader.MAP_NAME == null) {
                return "Unknown";
            }

            return MapLoader.MAP_NAME;
        });
    }

    public static void showServer()
    {
        ServerHookAPI.setDisplay(true);
    }

    public static void hideServer()
    {
        ServerHookAPI.setDisplay(false);
    }

    public static void teleportPlayer(String player)
    {
        String lobby = lobbies.get(random.nextInt(lobbies.size()));

        ServerHookAPI.teleportPlayer(player, lobby);
    }

    public static void teleportPlayer(Player player)
    {
        teleportPlayer(player.getName());
    }
}
